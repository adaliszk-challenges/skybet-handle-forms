<?php
namespace SkyBet\TechDemo\Api;

use Laravel\Lumen\Application as LumenApplication;

/**
 * API Application
 *
 * it creates the API services for a web endpoint
 *
 * @package SkyBet\TechTest\Api
 */
class Application extends LumenApplication
{

}
