<?php
namespace SkyBet\TechDemo\Providers;

use SkyBet\TechDemo\Abstracts\Entity as EntityInterface;
use InvalidArgumentException;
use DomainException;

/**
 * Simple provider for Entity classes
 */
abstract class Entity implements EntityInterface
{
    /** @var array of required properties for fill with data */
    protected $fillable = ['id'];

    /** @var int */
    protected $id;

    /**
     * Initialize properties
     * @param iterable|object $dataSet
     */
    public function initializeProperties($dataSet)
    {
        if (is_object($dataSet)) $dataSet = (array) $dataSet;
        if (!is_array($dataSet))
            throw new InvalidArgumentException("Invalid dataType!");

        // Quickly load the data values into the class properties
        foreach ($dataSet as $key => $value)
        {
            if (!in_array($key, $this->fillable))
                throw new InvalidArgumentException("Undefined property {$key} in ".implode(',', $this->fillable));

            $idx = array_search($key, $this->fillable);
            unset($this->fillable[$idx]);
            $this->$key = $value;
        }

        if (count($this->fillable) > 0)
            throw new DomainException("Missing data during initialization: ".implode(',', $this->fillable));
    }

    /**
     * Creating a new instance from the same Entity
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @param iterable|null $initialData
     * @return EntityInterface
     */
    public abstract function newInstance(?iterable $initialData = null): EntityInterface;

    /**
     * Getter for the ID property
     *
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * Serialize object
     */
    public function serialize()
    {
        serialize($this);
    }

    /**
     * Recover for serialized state
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        unserialize($serialized);
    }
}
